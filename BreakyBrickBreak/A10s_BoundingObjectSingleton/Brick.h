#pragma once
#include "GameObject.h"
class Brick :
	public GameObject
{
protected:
	vector3 color;
	int numBrick;

public:
	Brick(String identifier);
	Brick(String identifier, vector3 a_v3Position, vector3 a_v3Rotation, vector3 a_v3Scale, int a_numBrick);

	void SetColor(vector3 color);
	vector3 GetColor();

	int GetNum() override;
	void SetNum(int a_numBrick) override;
	char GetType() override;

	virtual ~Brick();
};

