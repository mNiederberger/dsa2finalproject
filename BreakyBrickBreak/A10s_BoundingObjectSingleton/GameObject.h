#pragma once

#include "RE\ReEngAppClass.h"
#include "Transform.h"
#include "MyBOClass.h"
#include "MyBOManager.h"

class GameObject
{
public:
#pragma region variables
	Transform transform;
#pragma endregion 

#pragma region big5
	GameObject(String identifier);
	GameObject(String identifier, vector3 a_v3Position, vector3 a_v3Rotation, vector3 a_v3Scale);
	virtual ~GameObject();
#pragma endregion

#pragma region functions
	MyBOClass *GetBoundingObject();
	String GetID();
	/*void SetModelID(String model);
	String GetModelID();*/
	void SetMeshMngr(MeshManagerSingleton* meshMngrIn);
	void SetBOMngr(MyBOManager* bOMngrIn);
	void SetBo(bool a_lsetBo);
	virtual void Update();
	virtual void Render();
	virtual char GetType();
	virtual int GetNum();
	virtual void SetNum(int a_numBrick);
	virtual void Destroy(); 
#pragma endregion


protected:
#pragma region variables
	String name;
	//MyBOClass* boundingObject;
	MyBOManager* m_pBOMngr;
	MeshManagerSingleton* m_pMeshManager;
	//String modelID;
	bool bOSet = false;
#pragma endregion

#pragma region functions
#pragma endregion
};

