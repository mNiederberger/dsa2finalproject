#pragma once

#include "RE\ReEngAppClass.h"
#include <SFML\Graphics.hpp>
#include <vector>
#include <map>
#include "MyBOClass.h"
#include "MyBOManager.h"
#include "Ball.h"
#include "Brick.h"
#include "Player.h"
#include "Walls.h"

using namespace ReEng; //Using ReEng namespace to use all the classes in the dll

enum class GameState {Playing, Paused, ReadyToPlay, Loading, GameOver};

class GameManager
{
	//uint m_nObjectCount = 0;
	static GameManager* instance;
	MyBOManager* m_pBOManager;
	std::vector<GameObject*> m_lGameObject;
	std::vector<String> m_lLevel;
	std::map<String, uint> m_mapIndex;

	MeshManagerSingleton* m_pMeshMngr;

	Player player = Player();
	Ball ball = Ball("Ball", vector3(0.0f, 0.0f, -1.0f), vector3(0.0f), vector3(1.0f));
	
	std::vector<Walls> m_lWalls; // List of walls

	std::vector<Brick> m_lBricks; // List of bricks
	
	int m_nNumLevels = 5; //Change this if you add a new level.
	int m_nLevelCount = 0;

	std::vector<GameObject> m_lFrame;
	int m_nObjects = 20;

	GameState gameState = GameState::Loading;

public:
	
	static GameManager* GetInstance();
	static void ReleaseInstance(void);
	void HandleWallCollision(void);
	void GameManager::Release(void);
	void Update(void);

	GameState GetGameState();
	void GameManager::AddObject(GameObject* a_gameObj);
	void CollisionResolution(MyBOClass* a_pCollidingBO);
	void RemoveBrick(int a_iOther);
	void Scoring();
	void StartLevel(void);
	void PauseGame(void);
	void ManageLives(int changeLives);
	void ManageScore(int changeScore);

	int numLives = 3;
	int score = 0;

	Player* GetPlayer();
	String levelDescription;
	String levelTitle;

	void Init(void);

	void LoadLevel(String fileName);
	void UnloadLevel();

	GameManager();
	~GameManager();
};

