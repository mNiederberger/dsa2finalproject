#include "GameObject.h"

#pragma region big5
GameObject::GameObject(String identifier)
{
	name = identifier;
	transform = Transform();
}

GameObject::GameObject(String identifier, vector3 a_v3Position, vector3 a_v3Rotation, vector3 a_v3Scale)
{
	name = identifier;
	transform.position = a_v3Position;
	transform.rotation = glm::quat(a_v3Rotation);
	transform.scale = a_v3Scale;
}

GameObject::~GameObject()
{
}
#pragma endregion

#pragma region pubicFunctions
MyBOClass* GameObject::GetBoundingObject()
{
	return m_pBOMngr->GetObjectW(m_pBOMngr->GetIndex(name));
}

String GameObject::GetID()
{
	return name;
}

//String GameObject::GetModelID()
//{
//	return modelID;
//}

//void GameObject::SetModelID(String model)
//{
//	modelID = model;
//}

void GameObject::SetMeshMngr(MeshManagerSingleton* meshMngrIn)
{
	m_pMeshManager = meshMngrIn;
}

void GameObject::SetBOMngr(MyBOManager* bOMngrIn)
{
	m_pBOMngr = bOMngrIn;
}

void GameObject::SetBo(bool a_lsetBo)
{
	bOSet = a_lsetBo;
}

void GameObject::Update()
{
	if (bOSet)
		m_pBOMngr->SetModelMatrix(transform.GetGlobalTransform(), name);
}

void GameObject::Render()
{
	m_pMeshManager->SetModelMatrix(transform.GetGlobalTransform(), name);
	m_pMeshManager->AddInstanceToRenderList(name);
}

void GameObject::Destroy()
{
	m_pBOMngr->RemoveObject(name);
}

char GameObject::GetType()
{
	return ' ';
}

void GameObject::SetNum(int a_numBrick)
{

}

int GameObject::GetNum()
{
	return -1;
}
#pragma endregion


#pragma region privateFunctions
#pragma endregion
