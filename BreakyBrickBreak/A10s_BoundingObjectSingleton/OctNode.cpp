#include "OctNode.h"


OctNode::OctNode(vector3 _center, float _distance, OctNode* _parent)
{
	isLeaf = false;
	center = _center;
	distance = _distance;
	m_pMeshMngr = MeshManagerSingleton::GetInstance();
	parent = _parent;

	for (size_t i = 0; i < 8; i++)
	{
		children[i] = nullptr;
	}
}

bool OctNode::GenerateChildren(uint depth, std::vector<MyBOClass*> gameObjects)
{

	if (gameObjects.size() == 0)
	{
		GenerateLeaf();
		return false;
	}

	std::vector<vector3> cubeCoords;

	//Generate the corners of this node so I can make a BO of it
	cubeCoords.push_back(vector3(-0.5f, -0.5f, 0.5f));
	cubeCoords.push_back(vector3(0.5f, -0.5f, 0.5f));
	cubeCoords.push_back(vector3(0.5f, 0.5f, 0.5f));
	cubeCoords.push_back(vector3(-0.5f, 0.5f, 0.5f));
	cubeCoords.push_back(vector3(-0.5f, -0.5f, -0.5f));
	cubeCoords.push_back(vector3(0.5f, -0.5f, -0.5f));
	cubeCoords.push_back(vector3(0.5f, 0.5f, -0.5f));
	cubeCoords.push_back(vector3(-0.5f, 0.5f, -0.5f));

	glm::mat4 modelMatrix = glm::translate(center);
	modelMatrix = glm::scale(modelMatrix, distance, distance, distance);

	MyBOClass nodeBO = MyBOClass(cubeCoords);
	nodeBO.SetModelMatrix(modelMatrix);

	//MyBOManager* BOManager = MyBOManager::GetInstance();

	for (uint i = 0; i < gameObjects.size(); ++i)
	{
		if (nodeBO.IsColliding(gameObjects[i]))
		{
			//Add a pointer to the bricks
			objectsInNode.push_back(gameObjects[i]);
		}
	}

	if (depth <= 0 || objectsInNode.size() <= 3)
	{
		GenerateLeaf();
		return false;
	}

	//Half the cube coords so we can generate the new cubes
	for (uint i = 0; i < 8; ++i)
	{
		//Casting is magic :P
		cubeCoords[i] = (vector3)(vector4(cubeCoords[i] * 0.5f * distance, 1.0f)) + center;
	}

	for (uint i = 0; i < 8; ++i)
	{
		//Create a node at the locations
		children[i] = new OctNode(cubeCoords[i], distance * 0.5f, this);
		children[i]->GenerateChildren(depth - 1, objectsInNode);
	}

	return true;
}

void OctNode::GenerateLeaf()
{
	isLeaf = true;
}

void OctNode::DeleteObjFromTree(MyBOClass* bOOjb)
{
	for (uint i = 0; i < objectsInNode.size(); i++)
	{
		if (objectsInNode[i] == bOOjb)
		{
			if (!isLeaf)
			{
				for (uint nchild = 0; nchild < 8; nchild++)
				{
					children[nchild]->DeleteObjFromTree(bOOjb);
				}
			}

			objectsInNode.erase(objectsInNode.begin() + i);
		}
	}
}

OctNode* OctNode::GetLeafAtPoint(vector3 point)
{
	if (isLeaf)
		return this;

	vector3 point2 = point - center;

	//0000 0 >z >y >x
	byte flags = 0;

	if (point2.x > 0)
	{
		flags = 1;
	}
	if (point2.y > 0)
	{
		flags = flags | 2;
	}
	if (point2.z > 0)
	{
		flags = flags | 4;
	}

	switch (flags)
	{
	case 0:
		return children[4]->GetLeafAtPoint(point);
		break;
	case 1:
		return children[5]->GetLeafAtPoint(point);
		break;
	case 2:
		return children[7]->GetLeafAtPoint(point);
		break;
	case 3:
		return children[6]->GetLeafAtPoint(point);
		break;
	case 4:
		return children[0]->GetLeafAtPoint(point);
		break;
	case 5:
		return children[1]->GetLeafAtPoint(point);
		break;
	case 6:
		return children[3]->GetLeafAtPoint(point);
		break;
	case 7:
		return children[2]->GetLeafAtPoint(point);
		break;
	default:
		break;
	}

	return this;
}

std::vector<MyBOClass*> OctNode::GetObjsInNode()
{
	return objectsInNode;
}

void OctNode::DrawTree()
{
	if (isLeaf)
	{
		m_pMeshMngr->AddCubeToQueue(glm::translate(center) * glm::scale(distance, distance, distance), RERED, WIRE);
	}
	else
	{
		m_pMeshMngr->AddCubeToQueue(glm::translate(center) * glm::scale(distance, distance, distance), REBLUE, WIRE);
	}

	m_pMeshMngr->AddSphereToQueue(glm::translate(center), REGREEN, WIRE);

	if (!isLeaf)
	{
		for (uint i = 0; i < 8; ++i)
		{
			children[i]->DrawTree();
		}
	}
}

void OctNode::ClearTree()
{
	if (parent != nullptr)
	{
		parent->ClearTree();
	}
	else
	{
		KillBranch();
	}
}

void OctNode::KillBranch()
{
	for (uint i = 0; i < 8; i++)
	{
		if (children[i] != nullptr)
		{
			children[i]->KillBranch();
		}
	}
	for (uint i = 0; i < 8; i++)
	{
		if (children[i] != nullptr)
			delete children[i];
	}
}

OctNode::~OctNode()
{
	if (parent == nullptr)
	{
		ClearTree();
	}
}
