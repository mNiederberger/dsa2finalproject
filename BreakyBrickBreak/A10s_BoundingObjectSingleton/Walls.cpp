#include "Walls.h"

Walls::Walls(String a_sIdentifier)
: GameObject(a_sIdentifier)
{
}

Walls::Walls(String a_sIdentifier, vector3 a_v3Position, vector3 a_v3Rotation, vector3 a_v3Scale)
: GameObject(a_sIdentifier, a_v3Position, a_v3Rotation, a_v3Scale)
{
}

vector3 Walls::GetColor()
{
	return color;
}

void Walls::SetColor(vector3 a_v3color)
{
	color = a_v3color;
}

char Walls::GetType()
{
	return 'w';
}

Walls::~Walls()
{
}
