#pragma once
#include "GameObject.h"

class Ball :
	public GameObject
{
	float m_fMass;
	vector3 m_v3Velocity;

public:
	Ball(String identifier);
	Ball(String identifier, vector3 a_v3Position, vector3 a_v3Rotation, vector3 a_v3Scale);
	~Ball();

	void SetMass(float a_fMass);
	void SetVelocity(vector3 a_v3Velocity);

	void Update() override;
	void Render() override;

	virtual vector3 getVel();
};

