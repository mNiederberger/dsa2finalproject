#include "Player.h"


Player::Player() : GameObject("Player")
{
	
}


Player::~Player()
{
}

char Player::GetType()
{
	return 'p';
}

void Player::Render()
{
	std::vector<vector3> verts = m_pMeshManager->GetVertexList(name);
	m_pMeshManager->AddLineToRenderList(verts[verts.size() - 1], verts[0], RERED);
	for (size_t i = 0; i < verts.size() - 1; i++)
	{
		m_pMeshManager->AddLineToRenderList(verts[i], verts[i + 1], RERED);
	}
}
