#pragma once
#include "GameObject.h"
class Walls :
	public GameObject
{
public:
	Walls(String a_sIdentifier);
	Walls(String a_sIdentifier, vector3 a_v3Position, vector3 a_v3Rotation, vector3 a_v3Scale);

	void SetColor(vector3 color);
	vector3 GetColor();
	char GetType() override;

	vector3 color;

	~Walls();
};

