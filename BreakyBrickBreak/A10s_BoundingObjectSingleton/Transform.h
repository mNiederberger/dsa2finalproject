#pragma once

#include "RE\ReEngAppClass.h"

class Transform
{
public:
	vector3 position;
	glm::quat rotation;
	vector3 scale;

	glm::mat4 GetGlobalTransform();
	glm::mat4 GetInverseMat();

	Transform();
	~Transform();
};

