#include "Brick.h"


Brick::Brick(String identifier) : GameObject(identifier)
{
	
}

Brick::Brick(String identifier, vector3 a_v3Position, vector3 a_v3Rotation, vector3 a_v3Scale, int a_numBrick)
	: GameObject(identifier, a_v3Position, a_v3Rotation, a_v3Scale)
{
	numBrick = a_numBrick;
}

int Brick::GetNum()
{
	return numBrick;
}

void Brick::SetNum(int a_numBrick)
{
	numBrick = a_numBrick;
}

char Brick::GetType()
{
	return 'b';
}

vector3 Brick::GetColor()
{
	return color;
}

void Brick::SetColor(vector3 color)
{
	this->color = color;
}

Brick::~Brick()
{
}
