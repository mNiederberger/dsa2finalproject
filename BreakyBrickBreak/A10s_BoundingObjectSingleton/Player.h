#pragma once

#include "GameObject.h"

class Player :
	public GameObject
{
public:
	Player();
	~Player();
	void Render() override;
	char GetType() override;
};

