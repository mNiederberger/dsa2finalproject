#include "GameManager.h"

GameManager* GameManager::instance = nullptr;

void GameManager::LoadLevel(String fileName)
{
	gameState = GameState::Loading;

	std::ifstream fileStream = std::ifstream(fileName, std::ios::in);

	//Storage variables
	vector3 color = REBLUE;
	vector3 scale = vector3(0.5f, 0.5f, 0.5f);

	if (fileStream)
	{
		int brickCount = 0;
		while (fileStream && !fileStream.eof())
		{
			char c;
			String line;
			std::getline(fileStream, line);
			std::istringstream ss{ line };
			ss >> c;

			switch (c)
			{
			case 'b': //brick
			{
				try
				{
					float x, y, z;
					ss >> x >> y >> z;

					std::vector<vector3> tempVerts;
					tempVerts.push_back(vector3(-0.5f, -0.5f, 0.5f));
					tempVerts.push_back(vector3(0.5f, -0.5f, 0.5f));
					tempVerts.push_back(vector3(0.5f, 0.5f, 0.5f));
					tempVerts.push_back(vector3(-0.5f, 0.5f, 0.5f));
					tempVerts.push_back(vector3(-0.5f, -0.5f, -0.5f));
					tempVerts.push_back(vector3(0.5f, -0.5f, -0.5f));
					tempVerts.push_back(vector3(0.5f, 0.5f, -0.5f));
					tempVerts.push_back(vector3(-0.5f, 0.5f, -0.5f));
					std::stringstream ss;
					ss << "b" << brickCount;
					String s = ss.str();
					m_lBricks.push_back(Brick(s, vector3(x, y, z), vector3(0, 0, 0), scale, brickCount) );
					m_lBricks.back().SetColor(color);
					m_lBricks.back().SetBOMngr(m_pBOManager);
					m_pBOManager->AddObject(tempVerts, s);
					m_pBOManager->SetModelMatrix(m_lBricks.back().transform.GetGlobalTransform(), s);
					//std::cout << "This code is flawless.";
					++brickCount;
					std::cout << "Brick at " << x << " " << y << " " << z << " " << s << std::endl;
				}
				catch (std::exception& e)
				{
					std::cout << e.what() << std::endl;
				}
			}
				break;
			case 'c': //Define a color
			{
				try
				{
					float x, y, z;
					ss >> x >> y >> z;

					color = vector3(x, y, z);
					std::cout << "color " << x << " " << y << " " << z << std::endl;
				}
				catch (std::exception& e)
				{
					std::cout << e.what() << std::endl;
				}
			}
				break;
			case 's': //Define a scale for bricks
			{
				try
				{
					float x, y, z;
					ss >> x >> y >> z;

					scale = vector3(x, y, z);
				}
				catch (std::exception& e)
				{
					std::cout << e.what() << std::endl;
				}
			}
				break;
			case 't': //Level title
			{
				try
				{
					levelTitle = line.substr(2, line.length() - 2);
				}
				catch (std::exception& e)
				{
					std::cout << e.what() << std::endl;
				}
			}
				break;
			case 'd': //Level description
			{
				try
				{
					levelDescription = line.substr(2, line.length() - 2);
				}
				catch (std::exception& e)
				{
					std::cout << e.what() << std::endl;
				}
			}
				break;
			case '#': //Comments
			{

			}
				break;
			default:
			{
				std::cout << "Undefined starting character in following line: " << line << std::endl;
			}
				break;
			}
		}
	}
	else
	{
		throw std::runtime_error("Level file not found!");
	}

	//gameState = GameState::ReadyToPlay;
}

void GameManager::UnloadLevel()
{
	gameState = GameState::Loading;
	for (auto it = m_lGameObject.begin(); it != m_lGameObject.end(); ++it)
	{
		if ((**it).GetType() == 'b')
		{
			(**it).Destroy();
			m_lGameObject.erase(it);
			--it;
		}
	}
}

void GameManager::Init(void)
{
	//m_nObjectCount = 0;
	m_pBOManager = MyBOManager::GetInstance();

	m_pMeshMngr = MeshManagerSingleton::GetInstance();

	//Load Models
	m_pMeshMngr->LoadModel("TheEscape\\player.obj", "Player");
	m_pMeshMngr->LoadModel("Planets\\07_Uranus.obj", "Ball");
	
	//Import from app class
	//Instantiate player stuff
	m_pBOManager->AddObject(m_pMeshMngr->GetVertexList(player.GetID()), player.GetID());
	this->AddObject(&player);
	player.SetBo(true);

	gameState = GameState::Loading;

	//Instantiate ball stuff
	m_pBOManager->AddObject(m_pMeshMngr->GetVertexList(ball.GetID()), ball.GetID());
	this->AddObject(&ball);
	ball.SetBo(true);

	ball.transform.position = vector3(0.0f, 0.0f, -1.0f);

	// Populate the list with walls FRONT and BACK Walls are 13.5 x 9 and SIDE Walls are 20 x 9
	m_lWalls.push_back(Walls("LeftWall", vector3(-6.75f, 0.0f, -10.0f), glm::radians(vector3(0, -90, 0)), vector3(20.0f, 9.0f, 0.0f)));
	m_lWalls.push_back(Walls("RightWall", vector3(6.75f, 0.0f, -10.0f), glm::radians(vector3(0, 90, 0)), vector3(20.0f, 9.0f, 0.0f)));
	m_lWalls.push_back(Walls("BottomWall", vector3(0.0f, -4.5f, -10.0f), glm::radians(vector3(90, 90, 0)), vector3(20.0f, 13.5f, 0.0f)));
	m_lWalls.push_back(Walls("TopWall", vector3(0.0f, 4.5f, -10.0f), glm::radians(vector3(90, 90, 0)), vector3(20.0f, 13.5f, 0.0f)));
	m_lWalls.push_back(Walls("FrontWall", vector3(0.0f, 0.0f, 1.0f), glm::radians(vector3(0, 0, 0)), vector3(13.5f, 9.0f, 0.0f)));
	m_lWalls.push_back(Walls("BackWall", vector3(0.0f, 0.0f, -20.0f), glm::radians(vector3(0, 0, 0)), vector3(13.5f, 9.0f, 0.0f)));

	//Instantiate level names
	for (int i = 0; i < m_nNumLevels; i++)
	{
		std::stringstream ss;
		ss << "Data\\Levels\\level" << i + 1 << ".txt";
		String s = ss.str();
		m_lLevel.push_back(s);
	}

	// Add in wireframes for depth perception, could use instance rendering potentially
	for (int nObject = 0; nObject < m_nObjects; nObject++)
	{
		m_lFrame.push_back(GameObject("Frame " + std::to_string(nObject), vector3(0.0f, 0.0f, (1.0f * -nObject) - 0.5f), glm::radians(vector3(0, 0, 0)), vector3(13.4f, 8.9f, 1.0f)));
	}

	this->LoadLevel(m_lLevel[m_nLevelCount]);

	//Instantiate brick stuff
	for (int i = 0; i < m_lBricks.size(); i++)
	{
		this->AddObject(&m_lBricks[i]);
		m_lBricks[i].SetBo(true);
	}

	std::vector<MyBOClass*> brickRefs = std::vector<MyBOClass*>();

	for (int i = 0; i < m_lBricks.size(); ++i)
	{
		brickRefs.push_back(m_lBricks[i].GetBoundingObject());
	}
	m_pBOManager->BuildOctree(brickRefs);

	// Set player scale
	player.transform.scale = vector3(0.2f, 0.2f, 0.2f);
	player.SetBOMngr(m_pBOManager);
	player.SetMeshMngr(m_pMeshMngr);

	// Set balls various force stuff
	ball.transform.position = vector3(0.0f, 0.0f, -1.0f);
	ball.transform.scale = vector3(0.5f, 0.5f, 0.5f);
	ball.SetMass(1.0f);
	ball.SetVelocity(vector3(0.0f, 0.0f, -1.0f));

	gameState = GameState::ReadyToPlay;
}

GameManager* GameManager::GetInstance() //Singleton
{
	if (instance == nullptr){
		instance = new GameManager();
	}
	return instance;
}

void GameManager::Release(void)
{
	for (uint nObject = 0; nObject < m_lGameObject.size(); nObject++)
	{
		if (m_lGameObject[nObject] != nullptr)
		{
			delete m_lGameObject[nObject];
			m_lGameObject[nObject] = nullptr;
		}
	}
	m_lGameObject.clear();
}

void GameManager::ReleaseInstance(void)
{
	if (instance != nullptr)
	{
		delete instance;
		instance = nullptr;
	}
}

void GameManager::AddObject(GameObject* a_gameObj)
{
	if (a_gameObj != nullptr)
	{
		m_lGameObject.push_back(a_gameObj);//Add the Object
		m_mapIndex[a_gameObj->GetID()] = m_lGameObject.size(); //Add entry to the dictionary
	}
	//m_nObjectCount = m_lGameObject.size();
}

void GameManager::CollisionResolution(MyBOClass* a_pCollidingBO)
{
	//MyBOClass* ballBO = ball.GetBoundingObject();
	//MyBOClass* otherBO = other->GetBoundingObject();
	//Something here about if the object is a brick, kill the brick, if it's a wall, bounce, if it's the paddle, do paddle stuff.
	//Brick collisions
	
	int tempIndex = m_pBOManager->GetIndex(a_pCollidingBO->owner);
	if (m_lGameObject[tempIndex]->GetType() == 'b')
	{
		//Get location of brick relative to ball
		vector3 brickVec = m_lGameObject[tempIndex]->transform.position;
		vector3 tempBallVel = ball.getVel();
		//Get the absolute distances of ball to brick
		float xAbsPos = abs(ball.transform.position.x - brickVec.x);
		float yAbsPos = abs(ball.transform.position.y - brickVec.y);
		float zAbsPos = abs(ball.transform.position.z - brickVec.z);
		//Check which value is the biggest. Largest value = it hit on that side.
		if (xAbsPos > yAbsPos && xAbsPos > zAbsPos)
		{
			ball.SetVelocity(vector3(-tempBallVel.x, tempBallVel.y, tempBallVel.z));
		}
		else if (yAbsPos > xAbsPos && yAbsPos > zAbsPos)
		{
			ball.SetVelocity(vector3(tempBallVel.x, -tempBallVel.y, tempBallVel.z));
		}
		else if (zAbsPos > yAbsPos && zAbsPos > xAbsPos)
		{
			ball.SetVelocity(vector3(tempBallVel.x, tempBallVel.y, -tempBallVel.z));
		}
		//Change the properties of the ball
		//ball.SetVelocity(-ball.getVel());
		//Remove the brick
		this->ManageScore(100);
		m_pBOManager->RemoveObject(m_lGameObject[tempIndex]->GetID());
		RemoveBrick(tempIndex);
		m_lGameObject.erase(m_lGameObject.end() - 1);
	}

	//Player collisions
	else if (m_lGameObject[tempIndex]->GetType() == 'p')
	{
		//Change the properties of the ball
		//Add more advanced ball/player collision here
		//get differences of centers
		vector3 affectVec = vector3(m_lGameObject[tempIndex]->transform.position.x - ball.transform.position.x, m_lGameObject[tempIndex]->transform.position.y - ball.transform.position.y, 0);
		vector3 normalBounce = vector3(ball.getVel());
		normalBounce.z = -normalBounce.z;

		ball.SetVelocity(normalBounce - affectVec * 0.3f);

		//std::cout << "ball redirected";
	}
}

void GameManager::HandleWallCollision()
{
	float radius = m_pBOManager->GetObjectW(m_pBOManager->GetIndex(ball.GetID()))->GetHalfWidthG().x;
	//Resolution for the left wall.
	if (ball.transform.position.x - radius < m_lWalls[0].transform.position.x)
	{
		ball.SetVelocity(vector3(-ball.getVel().x, ball.getVel().y, ball.getVel().z));
		//ball.transform.position.x = ball.transform.position.x + radius + 0.01f;
	}
	//Resolution for the right wall.
	else if (ball.transform.position.x + radius > m_lWalls[1].transform.position.x)
	{
		ball.SetVelocity(vector3(-ball.getVel().x, ball.getVel().y, ball.getVel().z));
		//ball.transform.position.x = ball.transform.position.x - radius - 0.01f;
	}
	//Resolution for the bottom wall.
	if (ball.transform.position.y - radius < m_lWalls[2].transform.position.y)
	{
		ball.SetVelocity(vector3(ball.getVel().x, -ball.getVel().y, ball.getVel().z));
		//ball.transform.position.y = ball.transform.position.y + radius + 0.01f;
	}
	//Resolution for the top wall.
	else if (ball.transform.position.y + radius > m_lWalls[3].transform.position.y)
	{
		ball.SetVelocity(vector3(ball.getVel().x, -ball.getVel().y, ball.getVel().z));
		//ball.transform.position.y = ball.transform.position.y - radius - 0.01f;
	}
	//Resolution for the front wall. This is where lives will be added.
	if (ball.transform.position.z + radius > m_lWalls[4].transform.position.z)
	{
		ball.transform.position = vector3(0.0f, 0.0f, -1.0f);
		ball.SetVelocity(vector3(0.0f, 0.0f, -1.0f));
		//ball.transform.position.z = ball.transform.position.z - radius - 0.01f;
		gameState = GameState::ReadyToPlay;
		this->ManageLives(-1);
	}
	//Resolution for the back wall.
	else if (ball.transform.position.z - radius < m_lWalls[5].transform.position.z)
	{
		ball.SetVelocity(vector3(ball.getVel().x, ball.getVel().y, -ball.getVel().z));
		ball.transform.position.z = ball.transform.position.z + radius + 0.01f;
	}
}


void GameManager::Update(void)
{

	// Player update position
	m_pMeshMngr->SetModelMatrix(player.transform.GetGlobalTransform(), player.GetID());
	m_pBOManager->SetModelMatrix(player.transform.GetGlobalTransform(), player.GetID());
	
	//check if playing
	if (gameState == GameState::Playing)
	{
		// Update and draw ball
		ball.Update();
		// Should be moved to ball class once we figure out whats up with the singletons
		//m_pMeshMngr->AddSphereToQueue(ball.transform.GetGlobalTransform(), REWHITE, 1);
		m_pMeshMngr->SetModelMatrix(ball.transform.GetGlobalTransform(), ball.GetID());
		m_pBOManager->SetModelMatrix(ball.transform.GetGlobalTransform(), ball.GetID());

		//Set the model matrix to the Bounding Object

		//m_pBOManager->Update();//Update collision detection
		m_pBOManager->CheckCollisions(m_pBOManager->GetObjectW(m_pBOManager->GetIndex(ball.GetID())), m_pBOManager->GetObjectW(m_pBOManager->GetIndex(player.GetID())));

		//Perform Collision Resolution
		/*std::vector<int> collideingIndicies = m_pBOManager->GetCollidingVector("Ball");
		for (int i = 0; i < collideingIndicies.size(); i++)
		{
			this->CollisionResolution(collideingIndicies[i]);
		}*/
		for each(MyBOClass* tempBO in m_pBOManager->GetCollidingBOs())
		{
			CollisionResolution(tempBO);
		}


		this->HandleWallCollision();
	}
	else
	{
		m_pMeshMngr->AddTextLineToQueue(REGREEN, "Click anywhere to play");
	}

	//m_pBOMngr->DisplaySphere(-1, REWHITE);
	//m_pBOMngr->DisplayReAlligned();
	//m_pBOMngr->DisplayOriented(-1, REWHITE);

	//m_pBOManager->DisplayOctree();

	//Adds all loaded instance to the render list
	m_pMeshMngr->AddInstanceToRenderList("ALL");

	// Render every wall but the front wall so the camera isn't blocked, it's just there for collision
	for (int i = 0; i < m_lWalls.size(); ++i)
	{
		if (m_lWalls[i].GetID() != "FrontWall")
			m_pMeshMngr->AddPlaneToQueue(m_lWalls[i].transform.GetGlobalTransform(), m_lWalls[i].GetColor());
	}

	// Render frames of play area
	for (int i = 0; i < m_lFrame.size(); ++i)
	{
		m_pMeshMngr->AddCubeToQueue(m_lFrame[i].transform.GetGlobalTransform(), REGREEN, WIRE);
	}

	// Render bricks
	for (int i = 0; i < m_lBricks.size(); ++i)
	{
		m_pMeshMngr->AddCubeToQueue(m_lBricks[i].transform.GetGlobalTransform(), m_lBricks[i].GetColor(), SOLID);
	}

	m_pMeshMngr->AddCubeToQueue(glm::translate(vector3(0.0f, 0.0f, ball.transform.position.z)) * glm::scale(vector3(13.3f, 8.8f, 1.0f)), REWHITE, WIRE);

	//m_pBOManager->DisplaySphere();
	//m_pBOManager->DisplayOriented();

	if (m_lBricks.empty()){
		ball.SetVelocity(vector3(0.0f, 0.0f, -1.0f));
		ball.transform.position = vector3(0.0f, 0.0f, -0.5f);
		m_nLevelCount++;
		if (m_nLevelCount < m_lLevel.size())
		{
			this->LoadLevel(m_lLevel[m_nLevelCount]);

			std::vector<MyBOClass*> brickRefs = std::vector<MyBOClass*>();

			for (int i = 0; i < m_lBricks.size(); ++i)
			{
				brickRefs.push_back(m_lBricks[i].GetBoundingObject());
			}
			m_pBOManager->BuildOctree(brickRefs);

			gameState = GameState::ReadyToPlay;

			for (int i = 0; i < m_lBricks.size(); i++)
			{
				this->AddObject(&m_lBricks[i]);
				m_lBricks[i].SetBo(true);
			}
		}
	}
}

void GameManager::RemoveBrick(int a_iOther)
{
	uint index = m_lGameObject[a_iOther]->GetNum();
	//remove object references
	for (int i = index + 1; i < m_lBricks.size(); i++)
	{
		m_lBricks[i].SetNum(m_lBricks[i].GetNum() - 1);
	}
	m_mapIndex.erase(m_lBricks[index].GetID());
	m_lBricks.erase(m_lBricks.begin() + index);
	//update index map
	for (auto it = m_mapIndex.begin(); it != m_mapIndex.end(); ++it)
	{
		if (it->second > index) it->second--;
	}
}

void GameManager::ManageLives(int changeLives)
{
	numLives = numLives + changeLives;
	if (numLives < 1)
	{
		gameState = GameState::GameOver;
		ball.SetVelocity(vector3(0.0f, 0.0f, 0.0f));
		ball.transform.position = vector3(0.0f, 0.0f, -1.0f);
	}
}

void GameManager::ManageScore(int changeScore)
{
	score = score + changeScore;
}

void GameManager::StartLevel()
{
	if (gameState == GameState::ReadyToPlay || gameState == GameState::Paused)
	{
		gameState = GameState::Playing;
	}
}

void GameManager::PauseGame()
{
	gameState = GameState::Paused;
}

Player* GameManager::GetPlayer()
{
	return &player;
}

GameManager::GameManager()
{
}


GameManager::~GameManager()
{
	Release();
}

GameState GameManager::GetGameState()
{
	return gameState;
}