#include "MyBOClass.h"
//  MyBOClass
void MyBOClass::Init(void)
{
	m_m4ToWorld = IDENTITY_M4;

	m_v3Center = vector3(0.0f);
	m_v3Min = vector3(0.0f);
	m_v3Max = vector3(0.0f);

	m_v3CenterG = vector3(0.0f);
	m_v3MinG = vector3(0.0f);
	m_v3MaxG = vector3(0.0f);

	m_v3HalfWidth = vector3(0.0f);
	m_v3HalfWidthG = vector3(0.0f);

	m_fRadius = 0.0f;
}
void MyBOClass::Swap(MyBOClass& other)
{
	std::swap(m_m4ToWorld, other.m_m4ToWorld);

	std::swap(m_v3Center, other.m_v3Center);
	std::swap(m_v3Min, other.m_v3Min);
	std::swap(m_v3Max, other.m_v3Max);

	std::swap(m_v3CenterG, other.m_v3CenterG);
	std::swap(m_v3MinG, other.m_v3MinG);
	std::swap(m_v3MaxG, other.m_v3MaxG);

	std::swap(m_v3HalfWidth, other.m_v3HalfWidth);
	std::swap(m_v3HalfWidthG, other.m_v3HalfWidthG);

	std::swap(m_fRadius, other.m_fRadius);
}
void MyBOClass::Release(void)
{

}
//The big 3
MyBOClass::MyBOClass(std::vector<vector3> a_lVectorList)
{
	//Init the default values
	Init();

	//Count the points of the incoming list
	uint nVertexCount = a_lVectorList.size();

	//If there are none just return, we have no information to create the BS from
	if (nVertexCount == 0)
		return;

	//Max and min as the first vector of the list
	m_v3Max = m_v3Min = a_lVectorList[0];

	//Get the max and min out of the list
	for (uint nVertex = 1; nVertex < nVertexCount; nVertex++)
	{
		if (m_v3Min.x > a_lVectorList[nVertex].x) //If min is larger than current
			m_v3Min.x = a_lVectorList[nVertex].x;
		else if (m_v3Max.x < a_lVectorList[nVertex].x)//if max is smaller than current
			m_v3Max.x = a_lVectorList[nVertex].x;

		if (m_v3Min.y > a_lVectorList[nVertex].y) //If min is larger than current
			m_v3Min.y = a_lVectorList[nVertex].y;
		else if (m_v3Max.y < a_lVectorList[nVertex].y)//if max is smaller than current
			m_v3Max.y = a_lVectorList[nVertex].y;

		if (m_v3Min.z > a_lVectorList[nVertex].z) //If min is larger than current
			m_v3Min.z = a_lVectorList[nVertex].z;
		else if (m_v3Max.z < a_lVectorList[nVertex].z)//if max is smaller than current
			m_v3Max.z = a_lVectorList[nVertex].z;
	}

	//with the max and the min we calculate the center
	m_v3Center = (m_v3Min + m_v3Max) / 2.0f;

	m_fRadius = glm::distance(m_v3Center, m_v3Max);
	//another way of doing this, except that with it the sphere just surrounds the object making the
	//pretest kind of useless
	//m_fRadius = 0;
	//for (uint nVertex = 0; nVertex < nVertexCount; nVertex++)
	//{
	//	float fDistance = glm::distance(m_v3Center, a_lVectorList[nVertex]);
	//	if (m_fRadius < fDistance)
	//		m_fRadius = fDistance;
	//}

	//we calculate the distance between all the values of min and max vectors
	m_v3HalfWidth = (m_v3Max - m_v3Min) / 2.0f;

	m_v3MaxG = m_v3Max;
	m_v3MinG = m_v3Min;
	m_v3CenterG = m_v3Center;
	m_v3HalfWidthG = m_v3HalfWidth;
}
MyBOClass::MyBOClass(MyBOClass const& other)
{
	m_m4ToWorld = other.m_m4ToWorld;

	m_v3Center = other.m_v3Center;
	m_v3Min = other.m_v3Min;
	m_v3Max = other.m_v3Max;

	m_v3CenterG = other.m_v3CenterG;
	m_v3MinG = other.m_v3MinG;
	m_v3MaxG = other.m_v3MaxG;

	m_v3HalfWidth = other.m_v3HalfWidth;
	m_v3HalfWidthG = other.m_v3HalfWidthG;

	m_fRadius = other.m_fRadius;
}
MyBOClass& MyBOClass::operator=(MyBOClass const& other)
{
	if (this != &other)
	{
		Release();
		Init();
		MyBOClass temp(other);
		Swap(temp);
	}
	return *this;
}
MyBOClass::~MyBOClass(){ Release(); };
//Accessors
void MyBOClass::SetModelMatrix(matrix4 a_m4ToWorld)
{
	//If there are no changes in the Model Matrix there is no need
	//of doing further calculations
	if (m_m4ToWorld == a_m4ToWorld)
		return;

	m_m4ToWorld = a_m4ToWorld;
	//Calculate the vertex that makes the Object
	vector3 v3Corner[8];
	v3Corner[0] = vector3(m_v3Min.x, m_v3Min.y, m_v3Min.z);
	v3Corner[1] = vector3(m_v3Max.x, m_v3Min.y, m_v3Min.z);
	v3Corner[2] = vector3(m_v3Min.x, m_v3Max.y, m_v3Min.z);
	v3Corner[3] = vector3(m_v3Max.x, m_v3Max.y, m_v3Min.z);

	v3Corner[4] = vector3(m_v3Min.x, m_v3Min.y, m_v3Max.z);
	v3Corner[5] = vector3(m_v3Max.x, m_v3Min.y, m_v3Max.z);
	v3Corner[6] = vector3(m_v3Min.x, m_v3Max.y, m_v3Max.z);
	v3Corner[7] = vector3(m_v3Max.x, m_v3Max.y, m_v3Max.z);
	//Get vectors in global space
	for (uint nVertex = 0; nVertex < 8; nVertex++)
	{
		v3Corner[nVertex] = vector3(m_m4ToWorld * vector4(v3Corner[nVertex], 1.0f));
	}

	//Get the max and min out of the list
	m_v3MaxG = m_v3MinG = v3Corner[0];
	for (uint nVertex = 1; nVertex < 8; nVertex++)
	{
		if (m_v3MinG.x > v3Corner[nVertex].x) //If min is larger than current
			m_v3MinG.x = v3Corner[nVertex].x;
		else if (m_v3MaxG.x < v3Corner[nVertex].x)//if max is smaller than current
			m_v3MaxG.x = v3Corner[nVertex].x;

		if (m_v3MinG.y > v3Corner[nVertex].y) //If min is larger than current
			m_v3MinG.y = v3Corner[nVertex].y;
		else if (m_v3MaxG.y < v3Corner[nVertex].y)//if max is smaller than current
			m_v3MaxG.y = v3Corner[nVertex].y;

		if (m_v3MinG.z > v3Corner[nVertex].z) //If min is larger than current
			m_v3MinG.z = v3Corner[nVertex].z;
		else if (m_v3MaxG.z < v3Corner[nVertex].z)//if max is smaller than current
			m_v3MaxG.z = v3Corner[nVertex].z;
	}
	m_v3CenterG = (m_v3MinG + m_v3MaxG) / 2.0f;

	//we calculate the distance between all the values of min and max vectors
	m_v3HalfWidthG = (m_v3MaxG - m_v3MinG) / 2.0f;

	m_fRadius = glm::distance(m_v3CenterG, m_v3MaxG);
}
float MyBOClass::GetRadius(void){ return m_fRadius; }
matrix4 MyBOClass::GetModelMatrix(void){ return m_m4ToWorld; }
vector3 MyBOClass::GetCenterLocal(void){ return m_v3Center; }
vector3 MyBOClass::GetCenterGlobal(void){ return m_v3CenterG; }
vector3 MyBOClass::GetHalfWidth(void){ return m_v3HalfWidth; }
vector3 MyBOClass::GetHalfWidthG(void){ return m_v3HalfWidthG; }
//--- Non Standard Singleton Methods
bool MyBOClass::IsColliding(MyBOClass* const a_pOther)
{
	//Get all vectors in global space
	vector3 v3Min = vector3(m_m4ToWorld * vector4(m_v3Min, 1.0f));
	vector3 v3Max = vector3(m_m4ToWorld * vector4(m_v3Max, 1.0f));

	vector3 v3MinO = vector3(a_pOther->m_m4ToWorld * vector4(a_pOther->m_v3Min, 1.0f));
	vector3 v3MaxO = vector3(a_pOther->m_m4ToWorld * vector4(a_pOther->m_v3Max, 1.0f));

	/*
	Are they colliding?
	For Objects we will assume they are colliding, unless at least one of the following conditions is not met
	*/
	//first check the bounding sphere, if that is not colliding we can guarantee that there are no collision
	if ((m_fRadius + a_pOther->m_fRadius) < glm::distance(m_v3CenterG, a_pOther->m_v3CenterG))
		return false;

	//If the distance was smaller it might be colliding

	bool bColliding = true;

	//Check for X
	if (m_v3MaxG.x < a_pOther->m_v3MinG.x)
		bColliding = false;
	if (m_v3MinG.x > a_pOther->m_v3MaxG.x)
		bColliding = false;

	//Check for Y
	if (m_v3MaxG.y < a_pOther->m_v3MinG.y)
		bColliding = false;
	if (m_v3MinG.y > a_pOther->m_v3MaxG.y)
		bColliding = false;

	//Check for Z
	if (m_v3MaxG.z < a_pOther->m_v3MinG.z)
		bColliding = false;
	if (m_v3MinG.z > a_pOther->m_v3MaxG.z)
		bColliding = false;
	//Separation Axis Theorem Test
	std::vector<vector3> normals = GetOrientedBoundingNormals();
	std::vector<vector3> oNormals = a_pOther->GetOrientedBoundingNormals();

	// Get the corners
	vector3 corners[8];
	corners[0] = vector3(m_v3Min.x, m_v3Min.y, m_v3Min.z);
	corners[1] = vector3(m_v3Max.x, m_v3Min.y, m_v3Min.z);
	corners[2] = vector3(m_v3Min.x, m_v3Max.y, m_v3Min.z);
	corners[3] = vector3(m_v3Max.x, m_v3Max.y, m_v3Min.z);

	corners[4] = vector3(m_v3Min.x, m_v3Min.y, m_v3Max.z);
	corners[5] = vector3(m_v3Max.x, m_v3Min.y, m_v3Max.z);
	corners[6] = vector3(m_v3Min.x, m_v3Max.y, m_v3Max.z);
	corners[7] = vector3(m_v3Max.x, m_v3Max.y, m_v3Max.z);
	//Get vectors in global space
	for (uint nVertex = 0; nVertex < 8; nVertex++)
	{
		corners[nVertex] = vector3(m_m4ToWorld * vector4(corners[nVertex], 1.0f));
	}

	vector3 oCorners[8];
	oCorners[0] = vector3(a_pOther->m_v3Min.x, a_pOther->m_v3Min.y, a_pOther->m_v3Min.z);
	oCorners[1] = vector3(a_pOther->m_v3Max.x, a_pOther->m_v3Min.y, a_pOther->m_v3Min.z);
	oCorners[2] = vector3(a_pOther->m_v3Min.x, a_pOther->m_v3Max.y, a_pOther->m_v3Min.z);
	oCorners[3] = vector3(a_pOther->m_v3Max.x, a_pOther->m_v3Max.y, a_pOther->m_v3Min.z);

	oCorners[4] = vector3(a_pOther->m_v3Min.x, a_pOther->m_v3Min.y, a_pOther->m_v3Max.z);
	oCorners[5] = vector3(a_pOther->m_v3Max.x, a_pOther->m_v3Min.y, a_pOther->m_v3Max.z);
	oCorners[6] = vector3(a_pOther->m_v3Min.x, a_pOther->m_v3Max.y, a_pOther->m_v3Max.z);
	oCorners[7] = vector3(a_pOther->m_v3Max.x, a_pOther->m_v3Max.y, a_pOther->m_v3Max.z);
	//Get vectors in global space
	for (uint nVertex = 0; nVertex < 8; nVertex++)
	{
		oCorners[nVertex] = vector3(a_pOther->GetModelMatrix() * vector4(oCorners[nVertex], 1.0f));
	}

	// Check against this objects normals
	for (size_t i = 0; i < normals.size(); i++)
	{
		float max, min, oMax, oMin;
		max = min = glm::dot(normals[i], corners[0]);
		oMax = oMin = glm::dot(normals[i], oCorners[0]);
		//project onto normals of this object
		for (size_t j = 1; j < 8; j++)
		{
			float tempDot = glm::dot(normals[i], corners[j]);
			if (max < tempDot)
			{
				max = tempDot;
			}
			else if (min > tempDot)
			{
				min = tempDot;
			}
		}
		//project other object onto normals of this object
		for (size_t j = 1; j < 8; j++)
		{
			float tempDot = glm::dot(normals[i], oCorners[j]);
			if (oMax < tempDot)
				oMax = tempDot;
			else if (oMin > tempDot)
				oMin = tempDot;
		}


		//check if maxis and mins overlap
		if (max < oMin || min > oMax)
			return false;
	}

	//Check using the other objects normals
	for (size_t i = 0; i < oNormals.size(); i++)
	{
		float max, min, oMax, oMin;
		max = min = glm::dot(oNormals[i], corners[0]);
		oMax = oMin = glm::dot(oNormals[i], oCorners[0]);
		//project onto normals of other object
		for (size_t j = 1; j < 8; j++)
		{
			float tempDot = glm::dot(oNormals[i], corners[j]);
			if (max < tempDot)
				max = tempDot;
			else if (min > tempDot)
				min = tempDot;
		}
		//project other object onto normals of other object
		for (size_t j = 1; j < 8; j++)
		{
			float tempDot = glm::dot(oNormals[i], oCorners[j]);
			if (oMax < tempDot)
				oMax = tempDot;
			else if (oMin > tempDot)
				oMin = tempDot;
		}


		//check if maxis and mins overlap

		if (max < oMin || min > oMax)
			return false;
	}

	//check plane between centroids
	//get vector between centroids
	vector3 normalFromCentroid = glm::normalize(((m_v3MaxG + m_v3MinG) * 0.5f) - ((a_pOther->m_v3MaxG + a_pOther->m_v3MinG) * 0.5f));
	MeshManagerSingleton::GetInstance()->AddLineToRenderList((m_v3MaxG + m_v3MinG) * 0.5f, (a_pOther->m_v3MaxG + a_pOther->m_v3MinG) * 0.5f);
	float max, min, oMax, oMin;
	max = min = glm::dot(normalFromCentroid, corners[0]);
	oMax = oMin = glm::dot(normalFromCentroid, oCorners[0]);
	//project onto normals of other object
	for (size_t j = 1; j < 8; j++)
	{
		float tempDot = glm::dot(normalFromCentroid, corners[j]);
		if (max < tempDot)
			max = tempDot;
		else if (min > tempDot)
			min = tempDot;
	}
	//project other object onto normals of other object
	for (size_t j = 1; j < 8; j++)
	{
		float tempDot = glm::dot(normalFromCentroid, oCorners[j]);
		if (oMax < tempDot)
			oMax = tempDot;
		else if (oMin > tempDot)
			oMin = tempDot;
	}

	//check if maxis and mins overlap

	if (max < oMin || min > oMax)
		return false;


	//has passed all tests, return true
	return true;
}

std::vector<vector3> MyBOClass::GetOrientedBoundingNormals()
{
	std::vector<vector3> normals = std::vector<vector3>();
	normals.push_back(glm::normalize(vector3(m_m4ToWorld * vector4(1.0f, 0.0f, 0.0f, 1.0f)) - vector3(m_m4ToWorld * vector4(0.0f, 0.0f, 0.0f, 1.0f))));
	normals.push_back(glm::normalize(vector3(m_m4ToWorld * vector4(0.0f, 1.0f, 0.0f, 1.0f)) - vector3(m_m4ToWorld * vector4(0.0f, 0.0f, 0.0f, 1.0f))));
	normals.push_back(glm::normalize(vector3(m_m4ToWorld * vector4(0.0f, 0.0f, 1.0f, 1.0f)) - vector3(m_m4ToWorld * vector4(0.0f, 0.0f, 0.0f, 1.0f))));

	return normals;
}