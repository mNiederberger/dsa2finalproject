#pragma once
#include <vector>
//#include "GameObject.h"
#include "MyBOClass.h"
#include <RE/ReEng.h>

class OctNode
{
private:
	OctNode* children[8];
	OctNode* parent = nullptr;
	bool isLeaf = false;

	vector3 center;
	float distance;

	std::vector<MyBOClass*> objectsInNode;

	MeshManagerSingleton* m_pMeshMngr;

	//removes all children
	void KillBranch();

public:
	OctNode(vector3 _center, float _distance, OctNode* _parent = nullptr);

	//void FindObjectsInNode(std::vector<Brick*>* bricks, std::vector<vector3> cubeCoords);
	
	void GenerateLeaf();

	//Clears all data out from octree and deletes everything
	void ClearTree();

	//Returns false if it creates a leaf
	bool GenerateChildren(uint depth, std::vector<MyBOClass*> gameObjects);

	//traverses tree and removes object from all nodes
	void DeleteObjFromTree(MyBOClass* bOOjb);

	OctNode* GetLeafAtPoint(vector3 point);

	std::vector<MyBOClass*> GetObjsInNode();
	
	void DrawTree();

	virtual ~OctNode();
};

