#include "AppClass.h"
void AppClass::ProcessKeyboard(void)
{
	bool bModifier = false;
	float fSpeed = 0.01f;

#pragma region ON PRESS/RELEASE DEFINITION
	static bool	bLastF1 = false, bLastF2 = false, bLastF3 = false, bLastF4 = false, bLastF5 = false,
				bLastF6 = false, bLastF7 = false, bLastF8 = false, bLastF9 = false, bLastF10 = false,
				bLastEscape = false, bLastF = false;
#define ON_KEY_PRESS_RELEASE(key, pressed_action, released_action){  \
			bool pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::key);			\
			if(pressed){											\
				if(!bLast##key) pressed_action;}/*Just pressed? */\
			else if(bLast##key) released_action;/*Just released?*/\
			bLast##key = pressed; } //remember the state
#pragma endregion

#pragma region Modifiers
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift))
		bModifier = true;
#pragma endregion

#pragma region Camera Positioning
	if(bModifier)
		fSpeed *= 10.0f;
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		m_pCameraMngr->MoveForward(fSpeed);

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		m_pCameraMngr->MoveForward(-fSpeed);
	
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		m_pCameraMngr->MoveSideways(-fSpeed);

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		m_pCameraMngr->MoveSideways(fSpeed);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		m_pCameraMngr->MoveVertical(-fSpeed);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
		m_pCameraMngr->MoveVertical(fSpeed);

	//pause game
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
		m_pGameManager->PauseGame();
#pragma endregion

#pragma region Player Control
	/*if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		player.transform.position.x -= 0.1f;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		player.transform.position.x += 0.1f;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		player.transform.position.y -= 0.1f;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		player.transform.position.y += 0.1f;*/
#pragma endregion

#pragma region Other Actions
	ON_KEY_PRESS_RELEASE(Escape, NULL, PostMessage(m_pWindow->GetHandler(), WM_QUIT, NULL, NULL));
	ON_KEY_PRESS_RELEASE(F1, NULL, m_pCameraMngr->SetCameraMode(CAMPERSP));
	ON_KEY_PRESS_RELEASE(F2, NULL, m_pCameraMngr->SetCameraMode(CAMROTHOZ));
	ON_KEY_PRESS_RELEASE(F3, NULL, m_pCameraMngr->SetCameraMode(CAMROTHOY));
	ON_KEY_PRESS_RELEASE(F4, NULL, m_pCameraMngr->SetCameraMode(CAMROTHOX));
#pragma endregion
}
void AppClass::ProcessMouse(void)
{
	m_bArcBall = false;
	m_bFPC = false;
	
	

	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)){
		m_pGameManager->StartLevel();
	}
	
	if(sf::Mouse::isButtonPressed(sf::Mouse::Button::Right))
		m_bFPC = true;


	//change the mouseloc locations from 0 to screen size to  from -1 to 1
	uint mouseX, mouseY, centerX, centerY;
	vector2 mouseOnWindow;
	centerX = m_pSystem->GetWindowX() + m_pSystem->GetWindowWidth() / 2;
	centerY = m_pSystem->GetWindowY() + m_pSystem->GetWindowHeight() / 2;

	mouseX = sf::Mouse::getPosition().x;
	mouseY = sf::Mouse::getPosition().y;

	// convert x
	if (mouseX < centerX)
		mouseOnWindow.x = MapValue(float(mouseX), float(m_pSystem->GetWindowX()), float(centerX), -1.0f, 0.0f);
	else if (mouseX > centerX)
		mouseOnWindow.x = MapValue(float(mouseX), float(centerX), float(m_pSystem->GetWindowX() + m_pSystem->GetWindowWidth()), 0.0f, 1.0f);
	else mouseOnWindow.x = 0.0f;

	// convert y
	if (mouseY < centerY)
		mouseOnWindow.y = MapValue(float(mouseY), float(m_pSystem->GetWindowY()), float(centerY), 1.0f, 0.0f);
	else if (mouseY > centerY)
		mouseOnWindow.y = MapValue(float(mouseY), float(centerY), float(m_pSystem->GetWindowY() + m_pSystem->GetWindowHeight()), 0.0f, -1.0f);
	else mouseOnWindow.y = 0.0f;

	//adding the topbar of the window
	if (!m_pSystem->IsWindowFullscreen())
	{
		mouseOnWindow.y += 0.03f;
	}
	Player* p = m_pGameManager->GetPlayer();
	p->transform.position.x = MapValue(mouseOnWindow.x, 0.0f, 1.0f, 0.0f, 7.8f);
	p->transform.position.y = MapValue(mouseOnWindow.y, 0.0f, 1.0f, 0.0f, 5.5f);

	//std::cout << p->transform.position.x << std::endl;
	//std::cout << p->transform.position.y << std::endl;
}
