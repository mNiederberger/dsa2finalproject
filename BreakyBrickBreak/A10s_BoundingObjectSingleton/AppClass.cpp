#include "AppClass.h"

void AppClass::InitWindow(String a_sWindowName)
{
	super::InitWindow("The Escape"); // Window Name

	// Set the clear color based on Microsoft's CornflowerBlue (default in XNA)
	//if this line is in Init Application it will depend on the .cfg file, if it
	//is on the InitVariables it will always force it regardless of the .cfg
	m_v4ClearColor = vector4(0.0f, 0.0f, 0.0f, 0.0f);
}

void AppClass::InitVariables(void)
{
	//Initialize positions
	m_pCameraMngr->SetPosition(vector3(0.0f, 0.0f, 12.50f));

	//Load Models
	//m_pMeshMngr->LoadModel("TheEscape\\player.obj", "Player");

	m_pBOMngr = MyBOManager::GetInstance();
	m_pGameManager = GameManager::GetInstance();

	m_pGameManager->Init();
}

void AppClass::Update(void)
{
	//Update the system's time
	m_pSystem->UpdateTime();

	//Update the mesh manager's time without updating for collision detection
	m_pMeshMngr->Update(false);

	//First person camera movement
	/*if (m_bFPC == true)
		CameraRotation();*/

	ArcBall();

	//Set the model matrices for both objects and Bounding Spheres


	//Run the game manager update
	m_pGameManager->Update();

	//Indicate the FPS
	int nFPS = m_pSystem->GetFPS();
	//print info into the console
	//printf("FPS: %d            \r", nFPS);//print the Frames per Second
	//Print info on the screen
	m_pMeshMngr->PrintLine(m_pSystem->GetAppName(), REYELLOW);
	m_pMeshMngr->PrintLine(m_pGameManager->levelTitle, REYELLOW);
	m_pMeshMngr->PrintLine(m_pGameManager->levelDescription, REYELLOW);
	m_pMeshMngr->PrintLine("Score: " + std::to_string(m_pGameManager->score), RERED);
	m_pMeshMngr->PrintLine("Lives: " + std::to_string(m_pGameManager->numLives), RERED);
	if (m_pGameManager->GetGameState() == GameState::GameOver)
	{
		m_pMeshMngr->PrintLine("\n\n																																GAMEOVER", RERED);
	}
}

void AppClass::Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the window

	////Render the grid based on the camera's mode:
	//switch (m_pCameraMngr->GetCameraMode())
	//{
	//default: //Perspective
	//	m_pMeshMngr->AddGridToQueue(1.0f, REAXIS::XY); //renders the XY grid with a 100% scale
	//	break;
	//case CAMERAMODE::CAMROTHOX:
	//	m_pMeshMngr->AddGridToQueue(1.0f, REAXIS::YZ, RERED * 0.75f); //renders the YZ grid with a 100% scale
	//	break;
	//case CAMERAMODE::CAMROTHOY:
	//	m_pMeshMngr->AddGridToQueue(1.0f, REAXIS::XZ, REGREEN * 0.75f); //renders the XZ grid with a 100% scale
	//	break;
	//case CAMERAMODE::CAMROTHOZ:
	//	m_pMeshMngr->AddGridToQueue(1.0f, REAXIS::XY, REBLUE * 0.75f); //renders the XY grid with a 100% scale
	//	break;
	//}
	
	m_pMeshMngr->Render(); //renders the render list

	m_pGLSystem->GLSwapBuffers(); //Swaps the OpenGL buffers
}

void AppClass::Release(void)
{
	super::Release(); //release the memory of the inherited fields
	MyBOManager::ReleaseInstance();
}