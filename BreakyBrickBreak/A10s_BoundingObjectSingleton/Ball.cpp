#include "Ball.h"

Ball::Ball(String a_sIdentifier)
: GameObject(a_sIdentifier)
{
}

Ball::Ball(String a_sIdentifier, vector3 a_v3Position, vector3 a_v3Rotation, vector3 a_v3Scale)
: GameObject(a_sIdentifier, a_v3Position, a_v3Rotation, a_v3Scale)
{
}

Ball::~Ball()
{
}

void Ball::Update()
{
	// Update the physic stuff of the ball
	m_v3Velocity.x = glm::clamp(m_v3Velocity.x, -.025f, .025f);
	m_v3Velocity.y= glm::clamp(m_v3Velocity.y, -.025f, .025f);
	m_v3Velocity.z = glm::clamp(m_v3Velocity.z, -.2f, .2f);

	transform.position = transform.position + m_v3Velocity;
}

void Ball::Render()
{
	//m_pMeshManager->SetModelMatrix(transform.GetGlobalTransform(), name);
	//m_pMeshManager->AddSphereToQueue(transform.GetGlobalTransform(), REWHITE, 1);
}

vector3 Ball::getVel(){ return m_v3Velocity; }
void Ball::SetMass(float a_fMass) { m_fMass = a_fMass; }
void Ball::SetVelocity(vector3 a_v3Velocity) { m_v3Velocity = a_v3Velocity; }