#include "Transform.h"

Transform::Transform()
{
	position = vector3(0);
	rotation = glm::quat(vector3(0));
	scale = vector3(1);
}


Transform::~Transform()
{
}

glm::mat4 Transform::GetGlobalTransform()
{
	return glm::translate(position) * glm::mat4_cast(rotation) * glm::scale(scale);
}

glm::mat4 Transform::GetInverseMat()
{
	return glm::scale(-scale) * glm::mat4_cast(-rotation) * glm::translate(-position);
}